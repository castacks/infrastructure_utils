#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# display usage
usage_msg="\
Usage: $(basename $0)

  options:
  --title <arg>
      Window title name
  --host <arg>
      RDP remote host IP (or host alias found in local /etc/hosts)
  [--user <arg>]
      RDP remote username
  [--pass <arg>]
      RDP remote user password
  [--res <arg>]
      RDP desktop resolution
  [--help]
      Shows usage message.
  [help]
      Shows usage message.

  Azure RPD Wrapper
    - Quick GoTo script, to establish a rdp session with an Azure VM.
    - Hides rdp 'rdesktop' command line cli options & details.

  * optional arguments are enclosed in square brackets

For more help, please see the README.md or wiki."

# //////////////////////////////////////////////////////////////////////////////
# load helper scripts

# load print-info
. "$(dirname $0)/.utils.bash"
validate "load utils failed"

# load print-info
. "$(dirname $0)/args/.azure-rdp-args.bash"
validate "load args failed"

# display usage message
is_display_usage "$usage_msg"

# verify user input arguments
verify_user_input_args

# get the directory of this script -- "build.bash"
script_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
pushd $script_path  # go so script path

# //////////////////////////////////////////////////////////////////////////////
# script
script_title "Azure Remote Desktop"
print_user_arguments

# //////////////////////////////////////////////////////////////////////////////
# execute rdp command
rdesktop \
  -u $user \
  -p $pass \
  -k pt \
  -g $res \
  -T $title \
  -N \
  -a 16 \
  -z \
  -xl \
  -r clipboard:CLIPBOARD \
  $host

# cleanup & exit
newline
popd
exit_success
